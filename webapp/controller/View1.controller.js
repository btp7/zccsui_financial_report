sap.ui.define([
    "sap/ui/core/mvc/Controller",
    "sap/ui/core/BusyIndicator"
],
    /**
     * @param {typeof sap.ui.core.mvc.Controller} Controller
     */
    function (Controller, BusyIndicator) {
        "use strict";

        return Controller.extend("zccs.uifinancialreport.controller.View1", {
            _aTableData: [],
            _aTableFilters: [],
            _oTableSort: {},
            _devicePhone: false,
            _deviceTablet: false,
            _selectedCount: "",
            _reportData: [],

            showBusyIndicator: function (iDuration, iDelay) {
                BusyIndicator.show(iDelay);

                if (iDuration && iDuration > 0) {
                    if (this._sTimeoutId) {
                        clearTimeout(this._sTimeoutId);
                        this._sTimeoutId = null;
                    }

                    this._sTimeoutId = setTimeout(function () {
                        this.hideBusyIndicator();
                    }.bind(this), iDuration);
                }
            },
            hideBusyIndicator: function () {
                BusyIndicator.hide();
            },

            onInit: function () {
                this.getOwnerComponent().setModel(new sap.ui.model.json.JSONModel({
                    appView: {
                        company: null,
                        yearfrom: null,
                        salesorg: null,
                        yearto: null,
                        customer: null,
                        applicationno: null
                    },
                    dialog: {
                        pic: null,
                        date: null,
                        no: null,
                        year: null,
                        currency: null,
                        unit: null,
                        dataattribute: null,
                        company: null,
                        customercode: null,
                        customername: null,
                        score: null,
                        class: null
                    },
                    treedata: [],
                    treecount: 10
                }));

                this._devicePhone = sap.ui.Device.system.phone;
                this._deviceTablet = sap.ui.Device.system.tablet && !sap.ui.Device.system.desktop;
            },
            onliveChangeRowsPage: function (oEvent) {
                if (oEvent.getSource().getValue().length > 2) {
                    oEvent.getSource().setValue(oEvent.getSource().getValue().slice(0, -1));
                }
            },

            onPressSearch: function () {

                var sfilter = "",
                    sApplication_no = this.getView().getModel().getProperty("/appView/applicationno"),
                    sCompany = this.getView().getModel().getProperty("/appView/company"),
                    sCustomer = this.getView().getModel().getProperty("/appView/customer"),
                    sSalesorg = this.getView().getModel().getProperty("/appView/salesorg"),
                    sYearfrom = this.getView().getModel().getProperty("/appView/yearfrom"),
                    sYearto = this.getView().getModel().getProperty("/appView/yearto");

                if (sApplication_no) {
                    sfilter = "?$filter=(application_no eq '" + sApplication_no + "')";
                }

                if (sCompany) {
                    if (sfilter) {
                        sfilter = sfilter + " and (company_code eq '" + sCompany + "')";
                    } else {
                        sfilter = "?$filter=(company_code eq '" + sCompany + "')";
                    }
                }

                if (sCustomer) {
                    if (sfilter) {
                        sfilter = sfilter + " and (customer_code eq '" + sCustomer + "')";
                    } else {
                        sfilter = "?$filter=(customer_code eq '" + sCustomer + "')";
                    }
                }

                if (sYearfrom && sYearto) {
                    if (sfilter) {
                        sfilter = sfilter + " and (year ge '" + sYearfrom + "' and year le '" + sYearto + "')";
                    } else {
                        sfilter = "?$filter=(year ge '" + sYearfrom + "' and year le '" + sYearto + "')";
                    }
                }

                var aFinancialDataBHeaders = {};
                $.ajax({
                    type: "get",
                    async: false,
                    contentType: "application/json",
                    url: this.getOwnerComponent().getManifestObject().resolveUri("odata/v4/FinancialDataService/FinancialDataBHeaders") + sfilter,
                    dataType: "json",
                    cache: false,
                    success: function (data, textStatus, jqXHR) { aFinancialDataBHeaders = data.value; }
                });


                this.getView().setModel(new sap.ui.model.json.JSONModel({ data: aFinancialDataBHeaders }), "dataModel");
                this.getView().getModel("dataModel").dataLoaded().then(
                    function (params) {
                        this._setTableSettings();
                    }.bind(this)
                )

            },
            _setTableSettings: function (params) {
                var oTable = this.getView().byId("idTable");

                var oTableModel = this.getView().getModel("tableItems");
                var aTableData = this.getView().getModel("dataModel").getData().data;
                oTableModel.setData(aTableData);

                this._reportData = aTableData;
                this.addPaginator("idTable", aTableData);
            },


            addPaginator: function (tableId, tableData) {


                var oTable = this.byId(tableId);
                var oContentHolder = oTable.getParent();


                this._destroyControl("selectPage");

                this._destroyControl("vbox1");
                var oVBox1 = new sap.m.VBox("vbox1", {
                });

                this._destroyControl("hbox1");
                var oHBox1 = new sap.m.HBox("hbox1", {
                    justifyContent: "SpaceBetween",
                    width: "90%"
                });


                this._destroyControl("hboxPagination");
                var oHBoxPagination = new sap.m.HBox("hboxPagination", {
                    justifyContent: "Center",
                    width: "75%"
                });


                this._destroyControl("hbox2");
                var oHBox2 = new sap.m.HBox("hbox2", {
                    justifyContent: "SpaceBetween",
                    width: "15%"
                });


                this._destroyControl("hbox3");
                var oHBox3 = new sap.m.HBox("hbox3", {
                    alignItems: "Center",
                    width: "45%"
                });

                this._destroyControl("label1");
                var oLabel1 = new sap.m.HBox("label1", {
                    text: "Kalem Sayısı"
                });


                this._destroyControl("hbox4");
                var oHBox4 = new sap.m.HBox("hbox4", {
                    width: "45%"
                });


                if (this._selectedCount === "") {
                    this._selectedCount = "10";
                }

                this._destroyControl("comboboxCount");
                var oComboBoxCount = new sap.m.ComboBox("comboboxCount", {
                    selectedKey: this._selectedCount,
                    width: "10em",
                    change: this.changeComboBoxCount.bind(this)
                });

                oComboBoxCount.addItem(new sap.ui.core.Item({ key: "10", text: "10" }));
                oComboBoxCount.addItem(new sap.ui.core.Item({ key: "25", text: "25" }));
                oComboBoxCount.addItem(new sap.ui.core.Item({ key: "50", text: "50" }));
                oComboBoxCount.addItem(new sap.ui.core.Item({ key: "100", text: "100" }));
                oComboBoxCount.addItem(new sap.ui.core.Item({ key: "150", text: "150" }));


                if (this._devicePhone) {
                    oHBoxPagination.setWidth("");
                    oHBox1.setJustifyContent("Center");
                    oHBox1.addItem(oHBoxPagination);
                    oHBox1.addItem(oLabel1);
                    oComboBoxCount.setSelectedKey("5");
                    // oHBox1.addItem(oComboBoxCount);
                    oVBox1.addItem(oHBox1);
                    oContentHolder.addContent(oVBox1);
                }

                else {
                    oHBox3.addItem(oLabel1);
                    oHBox4.addItem(oComboBoxCount);
                    oHBox2.addItem(oHBox3);
                    oHBox2.addItem(oHBox4);
                    oHBox1.addItem(oHBoxPagination);
                    oHBox1.addItem(oHBox2);
                    oVBox1.addItem(oHBox1);
                    oContentHolder.addContent(oVBox1);
                }

                this.generatePaginator(tableData);
                // oTable.addDelegate({
                // 	onAfterRendering: function () {
                // 		this.generatePaginator();
                // 	}.bind(this)
                // });
            },
            _destroyControl: function (id) {

                var oControl = this.getView().byId(id);
                if (oControl !== undefined) oControl.destroy();

                oControl = sap.ui.getCore().byId(id);
                if (oControl !== undefined) oControl.destroy();
            },

            changeComboBoxCount: function (oEvent) {

                var aTablex = this._reportData.slice(0);
                var aTable = this._sortAndFilterTable(aTablex);

                this._selectedCount = oEvent.getSource().getSelectedKey();

                this.generatePaginator(aTable);
            },

            _sortAndFilterTable: function (aData) {

                if (!(aData.length > 0)) return aData;


                for (var i = 0; i < this._aTableFilters.length; i++) {

                    var oTableFilters = this._aTableFilters[i];

                    var fieldType = typeof aData[0][oTableFilters.filterProp];

                    if (oTableFilters.filterValue !== null && oTableFilters.filterValue !== "") {
                        for (var k = 0; k < aData.length; k++) {
                            var tableRow = aData[k];

                            if (oTableFilters.filterOperator === "Contains") {
                                if (!((tableRow[oTableFilters.filterProp]).toString().includes(oTableFilters.filterValue))) {
                                    aData.splice(k, 1);
                                    k--;
                                }

                            }
                            else if (oTableFilters.filterOperator === "EQ") {

                                if (fieldType === "object") {

                                    var oDateFormat = sap.ui.core.format.DateFormat.getDateInstance({
                                        pattern: "dd.M.yyyy"
                                    });

                                    var compValue = oDateFormat.format(tableRow[oTableFilters.filterProp])

                                    if (!(compValue === oTableFilters.filterValue)) {
                                        aData.splice(k, 1);
                                        k--;
                                    }
                                }
                                else {
                                    if (!((tableRow[oTableFilters.filterProp]).toString() === oTableFilters.filterValue)) {
                                        aData.splice(k, 1);
                                        k--;
                                    }
                                }
                            }
                        }
                    }
                }
                // }

                if (!(aData.length > 0)) return aData;

                if (this._oTableSort.sortProperty !== undefined) {
                    aData.sort(function (a, b) {

                        switch (fieldType) {
                            case "string":
                                var textA = a[this._oTableSort.sortProperty].toUpperCase();
                                var textB = b[this._oTableSort.sortProperty].toUpperCase();

                                if (this._oTableSort.sortOrder === "Ascending") {
                                    return textA.localeCompare(textB);
                                    // return (a[this._oTableSort.sortProperty] - b[this._oTableSort.sortProperty])
                                }
                                else if ((this._oTableSort.sortOrder === "Descending")) {
                                    return textB.localeCompare(textA);
                                    // return (b[this._oTableSort.sortProperty] - a[this._oTableSort.sortProperty])
                                }
                                // return textA.localeCompare(textB);
                                // return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
                                break;
                            default:

                                if (this._oTableSort.sortOrder === "Ascending") {

                                    if (a[this._oTableSort.sortProperty] > b[this._oTableSort.sortProperty]) {
                                        return 1;
                                    }
                                    if (b[this._oTableSort.sortProperty] > a[this._oTableSort.sortProperty]) {
                                        return -1;
                                    }
                                    return 0;

                                    // return (a[this._oTableSort.sortProperty] - b[this._oTableSort.sortProperty])
                                }
                                else if ((this._oTableSort.sortOrder === "Descending")) {
                                    if (a[this._oTableSort.sortProperty] > b[this._oTableSort.sortProperty]) {
                                        return -1;
                                    }
                                    if (b[this._oTableSort.sortProperty] > a[this._oTableSort.sortProperty]) {
                                        return 1;
                                    }
                                    return 0;
                                    // return (b[this._oTableSort.sortProperty] - a[this._oTableSort.sortProperty])
                                }

                        }

                    }.bind(this));
                }

                return aData;


            },
            generatePaginator: function (tableData) {
                var oTablex = this.getView().byId("idTable");

                // var aData = oTablex.getModel("modelProducts").getData().ProductCollection;

                if (tableData === undefined)
                    return;

                var countTable = tableData.length;
                var oComboBoxCount = sap.ui.getCore().byId("comboboxCount");

                if (oComboBoxCount === undefined) { count = undefined; }
                else {

                    if (oComboBoxCount.getSelectedKey() !== undefined && oComboBoxCount.getSelectedKey() !== null) {
                        var count = parseInt(oComboBoxCount.getSelectedKey());
                    }
                    else {
                        count = undefined;
                    }
                }

                if (count !== undefined) {
                    var countPerPage = count;
                }
                else {

                    if (this._devicePhone) {
                        countPerPage = 5;
                    }
                    else {
                        countPerPage = 10;
                    }
                }


                oTablex.setVisibleRowCount(countPerPage);

                var size = parseInt(countTable / countPerPage);

                if (countTable % countPerPage !== 0) {
                    size++;
                }

                this.oPagination.container = sap.ui.getCore().byId("hboxPagination");
                this.oPagination.container.destroyItems();
                this.oPagination.init({
                    size: size,
                    page: 1,
                    step: 5,
                    table: oTablex,
                    countTable: countTable,
                    countPerPage: countPerPage,
                    tableData: tableData,
                    devicePhone: this._devicePhone,
                    deviceTablet: this._deviceTablet
                });
            },

            oPagination: {
                container: {},
                init: function (properties) {
                    this.Extend(properties);
                    this.Start();
                },

                Extend: function (properties) {
                    properties = properties || {};
                    this.size = properties.size || 1;
                    this.page = properties.page || 1;
                    this.step = properties.step || 5;
                    this.table = properties.table || {};
                    this.countTable = properties.countTable || 0;
                    this.countPerPage = properties.countPerPage || 10;
                    this.tableData = properties.tableData || 10;
                    this.devicePhone = properties.devicePhone;
                    this.deviceTablet = properties.deviceTablet;
                },

                Start: function () {

                    this.table.clearSelection();

                    this.container.destroyItems();


                    if (this.devicePhone || this.deviceTablet) {
                        var oSelect = new sap.m.Select("selectPage", {
                            change: this.SelectChange.bind(this),
                        });
                        this.container.addItem(oSelect);
                    }


                    if (this.size < this.step * 2 + 6) {
                        this.AddNumber(1, this.size + 1);
                    }

                    else if (this.page < this.step * 2 + 1) {
                        this.AddNumber(1, this.step * 2 + 4);
                        this.AddLastNumber();
                    }

                    else if (this.page > this.size - this.step * 2) {
                        this.AddFirstNumber();
                        this.AddNumber(this.size - this.step * 2 - 2, this.size + 1);
                    }
                    else {
                        this.AddFirstNumber();
                        this.AddNumber(this.page - this.step, this.page + this.step + 1);
                        this.AddLastNumber();
                    }

                    this.setFixedButtons();
                    if (this.devicePhone || this.deviceTablet) {
                        var aSelectItems = oSelect.getItems();

                        for (var k = 0; k < aSelectItems.length; k++) {
                            var item = aSelectItems[k];
                            var r = item.getText();

                            if (r === this.page.toString()) {
                                oSelect.setSelectedItem(item);
                            }
                        }
                    }

                    else {
                        var aButtons = this.container.getItems();
                        for (var i = 0; i < aButtons.length; i++) {
                            var oButton = aButtons[i];

                            if (oButton.getText() === this.page.toString()) {
                                oButton.setType("Emphasized");
                            }
                        }
                    }

                    this.filterTable();
                },

                AddNumber: function (s, f) {


                    for (var i = s; i < f; i++) {

                        if (this.devicePhone || this.deviceTablet) {
                            sap.ui.getCore().byId("selectPage").addItem(
                                new sap.ui.core.Item({
                                    key: i,
                                    text: i
                                })
                            );
                        }
                        else {

                            var oButton = new sap.m.Button({
                                text: i,
                                press: this.ClickNumber.bind(this)
                            });

                            this.container.addItem(oButton);
                        }
                    }
                },

                AddFirstNumber: function () {
                    if (this.devicePhone || this.deviceTablet) {
                        sap.ui.getCore().byId("selectPage").insertItem(
                            new sap.ui.core.Item({
                                key: 1,
                                text: 1
                            }, 2)
                        );
                    }
                    else {
                        var oButton = new sap.m.Button({
                            text: 1,
                            press: this.ClickNumber.bind(this)
                        });

                        this.container.insertItem(oButton, 2);

                        oButton = new sap.m.Button({
                            text: "...",
                            // press: this.Click.bind(this)
                        });
                        this.container.insertItem(oButton, 3);
                    }
                },
                AddLastNumber: function () {
                    if (this.devicePhone || this.deviceTablet) {
                        sap.ui.getCore().byId("selectPage").insertItem(
                            new sap.ui.core.Item({
                                key: this.size,
                                text: this.size
                            }, this.size - 3)
                        );
                    }
                    else {
                        var oButton = new sap.m.Button({
                            text: "...",
                            // press: this.ClickNumber.bind(this)
                        });

                        this.container.insertItem(oButton, this.size - 4);

                        oButton = new sap.m.Button({
                            text: this.size,
                            press: this.ClickNumber.bind(this)
                        });

                        this.container.insertItem(oButton, this.size - 3);
                    }
                },
                SelectChange: function (oEvent) {
                    this.page = parseInt(oEvent.getParameters().selectedItem.getText());
                    this.Start();
                },
                ClickNumber: function (oEvent) {
                    this.page = parseInt(oEvent.getSource().getText());
                    this.Start();
                },

                ClickPrev: function () {
                    this.page--;
                    if (this.page < 1) {
                        this.page = 1;
                    }
                    this.Start();
                },

                ClickNext: function () {
                    this.page++;
                    if (this.page > this.size) {
                        this.page = this.size;
                    }
                    this.Start();
                },

                ClickFirst: function () {
                    this.page = 1;
                    if (this.page < 1) {
                        this.page = 1;
                    }
                    this.Start();
                },

                ClickLast: function () {
                    this.page = this.size;
                    if (this.page > this.size) {
                        this.page = this.size;
                    }
                    this.Start();
                },


                setFixedButtons: function (e) {
                    if (this.devicePhone || this.deviceTablet) {
                        var oButton = new sap.m.Button({
                            icon: "sap-icon://close-command-field",
                            press: this.ClickFirst.bind(this)
                        });
                        this.container.insertItem(oButton, 0);

                        var oButton = new sap.m.Button({
                            icon: "sap-icon://navigation-left-arrow",
                            press: this.ClickPrev.bind(this)
                        });

                        this.container.insertItem(oButton, 1);

                        oButton = new sap.m.Button({
                            icon: "sap-icon://navigation-right-arrow",
                            press: this.ClickNext.bind(this)
                        });
                        this.container.insertItem(oButton, this.size + 2);

                        var oButton = new sap.m.Button({
                            icon: "sap-icon://open-command-field",
                            press: this.ClickLast.bind(this)
                        });
                        this.container.insertItem(oButton, this.size + 3);
                    }
                    else {

                        var oButton = new sap.m.Button({
                            text: "First",
                            press: this.ClickFirst.bind(this)
                        });
                        this.container.insertItem(oButton, 0);

                        oButton = new sap.m.Button({
                            text: "Next",
                            press: this.ClickNext.bind(this)
                        });
                        this.container.insertItem(oButton, 1);

                        oButton = new sap.m.Button({
                            text: "Previous",
                            press: this.ClickPrev.bind(this)
                        });
                        this.container.insertItem(oButton, this.size + 2);

                        oButton = new sap.m.Button({
                            text: "Last",
                            press: this.ClickLast.bind(this)
                        });
                        this.container.insertItem(oButton, this.size + 3);
                    }
                },

                filterTable: function () {

                    var aData = this.tableData;
                    var aDatax = [];


                    var indexes = {
                        start: (this.page - 1) * this.countPerPage,
                        end: (this.page - 1) * this.countPerPage + this.countPerPage - 1,
                    };

                    for (var index = 0; index < this.countTable; index++) {
                        if (indexes.start <= index && indexes.end >= index) {
                            var item = aData[index];
                            aDatax.push(item);
                        }
                    }
                    this.table.getModel("tableItems").setData(
                        { data: aDatax });

                }
            },

            _onLinkPress: function (oEvent) {
                /*      this.getView().setBusy(true); */
                var sObjectId = oEvent.getSource().getText();
                this._onGetFINHeader(sObjectId);
                this._onGetFINDetail(sObjectId);

                setTimeout(function () {
                    /*        this.showBusyIndicator(500, 0); */
                    this.byId("idDialog").open();
                }.bind(this), 700);

            },
            _onFinhisClose: function () {
                this.byId("idDialog").close();
            },

            _onGetFINHeader: function (sObjectId) {
                var sfilter = "?$filter=(application_no eq '" + sObjectId + "')";
                var aFinancialDataBHeaders = {};
                $.ajax({
                    type: "get",
                    async: false,
                    contentType: "application/json",
                    url: this.getOwnerComponent().getManifestObject().resolveUri("odata/v4/FinancialDataService/FinancialDataBHeaders") + sfilter,
                    dataType: "json",
                    cache: false,
                    success: function (data, textStatus, jqXHR) { aFinancialDataBHeaders = data.value; }
                });
                this.getView().getModel().setProperty("/dialog/pic", aFinancialDataBHeaders[0].pic);
                this.getView().getModel().setProperty("/dialog/date", aFinancialDataBHeaders[0].create_date);
                this.getView().getModel().setProperty("/dialog/no", aFinancialDataBHeaders[0].application_no);
                this.getView().getModel().setProperty("/dialog/year", aFinancialDataBHeaders[0].year);
                this.getView().getModel().setProperty("/dialog/currency", aFinancialDataBHeaders[0].currency);
                this.getView().getModel().setProperty("/dialog/unit", aFinancialDataBHeaders[0].unit);
                this.getView().getModel().setProperty("/dialog/dataattribute", aFinancialDataBHeaders[0].attribute);
                this.getView().getModel().setProperty("/dialog/company", aFinancialDataBHeaders[0].company_code);
                this.getView().getModel().setProperty("/dialog/customercode", aFinancialDataBHeaders[0].customer_code);
                this.getView().getModel().setProperty("/dialog/customername", aFinancialDataBHeaders[0].customer_name);
            },

            _onGetFINDetail: function (sObjectId) {
                var p1 = new Promise((resolve, reject) => {
                    $.ajax({
                        type: "get",
                        async: true,
                        contentType: "application/json",
                        url: this.getOwnerComponent().getManifestObject().resolveUri("odata/v4/MasterDataService/BigClasses"),
                        dataType: "json",
                        cache: false,
                        success: function (data, textStatus, jqXHR) {
                            resolve(data.value);
                        }
                    });
                });

                var p2 = new Promise((resolve, reject) => {
                    $.ajax({
                        type: "get",
                        async: true,
                        contentType: "application/json",
                        url: this.getOwnerComponent().getManifestObject().resolveUri("odata/v4/MasterDataService/MiddleClasses"),
                        dataType: "json",
                        cache: false,
                        success: function (data, textStatus, jqXHR) {
                            resolve(data.value);
                        }
                    });
                });

                var p3 = new Promise((resolve, reject) => {
                    $.ajax({
                        type: "get",
                        async: true,
                        contentType: "application/json",
                        url: this.getOwnerComponent().getManifestObject().resolveUri("odata/v4/MasterDataService/SmallClasses"),
                        dataType: "json",
                        cache: false,
                        success: function (data, textStatus, jqXHR) {
                            resolve(data.value);
                        }
                    });
                });

                var p4 = new Promise((resolve, reject) => {
                    $.ajax({
                        type: "get",
                        async: true,
                        contentType: "application/json",
                        url: this.getOwnerComponent().getManifestObject().resolveUri("odata/v4/MasterDataService/MiddleClassSortings"),
                        dataType: "json",
                        cache: false,
                        success: function (data, textStatus, jqXHR) {
                            resolve(data.value);
                        }
                    });
                });

                var p5 = new Promise((resolve, reject) => {
                    var sfilter = "?$filter=(application_no eq '" + sObjectId + "')";
                    $.ajax({
                        type: "get",
                        async: true,
                        contentType: "application/json",
                        url: this.getOwnerComponent().getManifestObject().resolveUri("odata/v4/FinancialDataService/FinancialDataItems") + sfilter,
                        dataType: "json",
                        cache: false,
                        success: function (data, textStatus, jqXHR) {
                            resolve(data.value);
                        }
                    });
                });

                var p6 = new Promise((resolve, reject) => {
                    $.ajax({
                        type: "get",
                        async: true,
                        contentType: "application/json",
                        url: this.getOwnerComponent().getManifestObject().resolveUri("odata/v4/MasterDataService/MiddleClassScores"),
                        dataType: "json",
                        cache: false,
                        success: function (data, textStatus, jqXHR) {
                            resolve(data.value);
                        }
                    });
                });


                var p7 = new Promise((resolve, reject) => {
                    $.ajax({
                        type: "get",
                        async: true,
                        contentType: "application/json",
                        url: this.getOwnerComponent().getManifestObject().resolveUri("odata/v4/MasterDataService/CustomerClasses"),
                        dataType: "json",
                        cache: false,
                        success: function (data, textStatus, jqXHR) {
                            resolve(data.value);
                        }
                    });
                });

                Promise.all([p1, p2, p3, p4, p5, p6, p7]).then((dataFromAll) => {

                    var aBigClasses = dataFromAll[0],
                        aMiddleClasses = dataFromAll[1],
                        aSmallClasses = dataFromAll[2],
                        aMiddleClassSortings = dataFromAll[3],
                        aFinancialDataItems = dataFromAll[4],
                        aMiddleClassScores = dataFromAll[5],
                        aCustomerClasses = dataFromAll[6];

                    var aData = { data: { categories: [] } },
                        aBigData = [],
                        sBigData = {},
                        aMiddleData = [],
                        sMiddleData = {},
                        aSmallData = [],
                        sSmallData = {},
                        aSortingsData = [],
                        sSortingsData = {},
                        aSubItem = [],
                        sSubItem = {},
                        aFinancialDataItemsData = [],
                        sFinancialDataItemsData = {},
                        aMiddleScoresData = [],
                        sMiddleScoresData = {},
                        aCustomerData = [],
                        sCustomerData = {},
                        sAmount = null,
                        sAmount_output = null,
                        sTotalScore = null,
                        sClass = null,
                        sFornula = null,
                        sFornula_copy = null,
                        sRange = null,
                        iCount = 0,
                        iTreeCount = 0,
                        pattern = new RegExp("[\u4E00-\u9FA5]+"),
                        pattern_en = new RegExp("[A-Za-z]+"),
                        pattern_number = new RegExp("/^[0-9] .?[0-9]*/"),
                        aSplitData = [],
                        sText = "",
                        sOrdering = null;

                    for (var i = 0; i < aBigClasses.length; i++) {
                        // Level 1
                        sBigData = {
                            name: aBigClasses[i].description_zf,
                            formula: null,
                            total: null,
                            score: null,
                            range: null,
                            categories: []
                        };

                        // Level 2
                        aMiddleData = [];
                        aMiddleClasses.filter(function (item_aMiddleClasses) {
                            if (item_aMiddleClasses.big_class_id === aBigClasses[i].id) {
                                if (item_aMiddleClasses.formula !== null) {
                                    sFornula = item_aMiddleClasses.formula;
                                    aSplitData = item_aMiddleClasses.formula.split(/[']/g);
                                    for (var d = 0; d < aSplitData.length; d++) {
                                        aSmallData = [];
                                        aSmallData = aSmallClasses.filter(function (item) {
                                            return item.id === aSplitData[d];
                                        });
                                        if (aSmallData.length !== 0) {
                                            sText = aSmallData[0].description_zf;
                                            sFornula = sFornula.replaceAll(aSplitData[d], sText);
                                        }
                                    }
                                    sFornula_copy = sFornula;
                                    if (sFornula) {
                                        sFornula = sFornula.replaceAll("'", "");
                                    }

                                    sMiddleData = {
                                        name: item_aMiddleClasses.description_zf,
                                        formula: sFornula,
                                        total: null,
                                        score: null,
                                        range: null
                                    };

                                    aMiddleData.push(sMiddleData);
                                    iCount = aMiddleData.length - 1;

                                    // Level 2-1
                                    aSubItem = [];
                                    if (sFornula !== null) {
                                        /*  var splitTable = sFornula_copy.split(/[']/g).filter(el => el); */
                                        var splitTable = item_aMiddleClasses.formula.split(/[']/g).filter(el => el);
                                        splitTable = splitTable.filter(function (item, pos) {
                                            return splitTable.indexOf(item) == pos;
                                        });
                                        for (var a = 0; a < splitTable.length; a++) {
                                             do {
                                                var iBracketsCount = 0;
                                                var iLocation = 0;
                                                for (var f = 0; f < splitTable[a].length; f++) {
                                                    if (splitTable[a].substr(f, 1) === "(") {
                                                        iBracketsCount += 1;
                                                        iLocation = f;
                                                    }
                                                    if (splitTable[a].substr(f, 1) === ")") {
                                                        iBracketsCount -= 1;
                                                        iLocation = f;
                                                    }
                                                }
                                                if (iBracketsCount !== 0) {
                                                    splitTable[a] = splitTable[a].substring(0, iLocation) + splitTable[a].substring(iLocation + 1, splitTable[a].length);
                                                }
                                            } while (iBracketsCount !== 0);
                                            if (isNaN(Number(splitTable[a])) === false) {
                                                continue;
                                            } 

                                            aSmallData = [];
                                            aSortingsData = [];
                                            aSmallData = aSmallClasses.filter(function (item_aSmallData) {
                                                /*           return item_aSmallData.description_zf === splitTable[a]; */
                                                return item_aSmallData.id === splitTable[a];
                                            });
                                            if (aSmallData.length === 0) {
                                                continue;
                                            }
                                            aSortingsData = aMiddleClassSortings.filter(function (item_aSortingsData) {
                                                return item_aSortingsData.middle_class_id === item_aMiddleClasses.id &&
                                                    item_aSortingsData.small_class_id === aSmallData[0].id;
                                            });

                                            if (aSortingsData.length === 0) {
                                                sOrdering = "000";
                                            } else {
                                                sOrdering = aSortingsData[0].ordering;
                                            }
                                            if (aSmallData.length > 0) {
                                                sSubItem = {
                                                    id: aSmallData[0].id,
                                                    /*   name: splitTable[a], */
                                                    name: aSmallData[0].description_zf,
                                                    sorting: sOrdering
                                                };
                                                aSubItem.push(sSubItem);
                                            }
                                        }
                                        aSubItem.sort(function (a, b) {
                                            return a.sorting - b.sorting;
                                        });

                                        for (var b = 0; b < aSubItem.length; b++) {
                                            aFinancialDataItemsData = [];
                                            aFinancialDataItemsData = aFinancialDataItems.filter(function (item_aFinancialDataItemsData) {
                                                return item_aFinancialDataItemsData.application_no === sObjectId &&
                                                    item_aFinancialDataItemsData.small_class_id === aSubItem[b].id;
                                            });

                                            if (aFinancialDataItemsData.length === 0 || aFinancialDataItemsData[0].amount === null) {
                                                sAmount = 0;
                                            } else {
                                                sAmount = aFinancialDataItemsData[0].amount;
                                            }
                                            sFornula = sFornula.replace(aSubItem[b].name, sAmount);

                                            if (sAmount < 0) {
                                                sAmount_output = "( " + Math.abs(sAmount) + " )";
                                            } else {
                                                sAmount_output = sAmount;
                                            }

                                            sMiddleData = {
                                                name: null,
                                                formula: aSubItem[b].name,
                                                total: sAmount_output,
                                                score: null,
                                                range: null
                                            };
                                            aMiddleData.push(sMiddleData);
                                        }

                                        // update total, score, range
                                        if (pattern.test(sFornula) || pattern_en.test(sFornula)) {
                                        } else {
                                            if (sFornula) {
                                                if (item_aMiddleClasses.percentage === "Y") {
                                                    sFornula = eval(sFornula) * 100;
                                                } else {
                                                    sFornula = eval(sFornula);
                                                }
                                                sFornula = sFornula.toFixed(2);
                                                sFornula = Number(sFornula);

                                                if (isNaN(Number(sFornula)) === true) {
                                                    sFornula = "";
                                                }

                                                if (sFornula < 0) {
                                                    sFornula = "( " + Math.abs(sFornula) + " )";
                                                }

                                                if (item_aMiddleClasses.percentage === "Y") {
                                                    aMiddleData[iCount].total = sFornula + " %";
                                                } else {
                                                    aMiddleData[iCount].total = sFornula;
                                                }

                                                aMiddleScoresData = aMiddleClassScores.filter(function (item_aMiddleScoresData) {
                                                    return item_aMiddleScoresData.middle_class_id === item_aMiddleClasses.id;
                                                });

                                                aMiddleScoresData.sort(function (a, b) {
                                                    return a.range - b.range;
                                                });

                                                for (var c = 0; c < aMiddleScoresData.length; c++) {
                                                    if (sFornula && sFornula <= aMiddleScoresData[c].range) {
                                                        sTotalScore = sTotalScore + aMiddleScoresData[c].score;
                                                        aMiddleData[iCount].score = aMiddleScoresData[c].score;
                                                        sRange = "0-" + aMiddleScoresData[c].range;
                                                        aMiddleData[iCount].range = sRange;
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        });

                        sBigData.categories = aMiddleData;
                        aBigData.push(sBigData);
                    }
                    // update total score, class
                    if (sTotalScore === 0 || sTotalScore === null) {
                        sTotalScore = null;
                        sClass = null;
                    } else {
                        aCustomerData = aCustomerClasses.sort(function (a, b) {
                            return a.score - b.score;
                        });
                        for (var d = 0; d < aCustomerData.length; d++) {
                            if (sTotalScore <= aCustomerData[d].score) {
                                sClass = aCustomerData[d].class_id + ":" + aCustomerData[d].description_zf;
                                break;
                            }
                        }
                    }
                    this.getView().getModel().setProperty("/dialog/score", sTotalScore);
                    this.getView().getModel().setProperty("/dialog/class", sClass);

                    aData.data.categories = aBigData;
                    this.getView().getModel().setProperty("/treedata", aData);
                    iTreeCount = 0;
                    iTreeCount = aData.data.categories.length;
                    for (var e = 0; e < aData.data.categories.length; e++) {
                        iTreeCount = iTreeCount + aData.data.categories[e].categories.length;
                    }
                    this.getView().getModel().setProperty("/treecount", iTreeCount);
                }).catch((err) => { console.log(err) });
            },
            onAfterRendering: function () {
                var oTreeTable = this.byId("TreeTableBasic");
                oTreeTable.expandToLevel(oTreeTable.getRows().length);
            },

        });
    });
