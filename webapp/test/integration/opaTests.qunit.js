/* global QUnit */

sap.ui.require(["zccs/uifinancialreport/test/integration/AllJourneys"
], function () {
	QUnit.config.autostart = false;
	QUnit.start();
});
